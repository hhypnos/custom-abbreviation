<h1>custom-abbreviation<br>create your own abbreviation for quick typing</h1>
<h2>Install</h2>
<ul>
<li>clone repository</li>
<li>install python3</li>
<li>install pip</li>
<li>install keyboard library from pip: <code>sudo pip install keyboard</code></li>
</ul>
<h2>usage</h2>
<ul>
<li>add <code>keyboard.add_abbreviation('yourAbbreviation','valueOfAbbreviation')</code> in config.py folder inside "settings" list<br>p.s separate it with comma</li>
<li>in terminal write: sudo python3 abbr.py</li>
<li>use your abbreviation anywhere and press space</li>
</ul>
<h3>Example</h3>
<code>settings=[
keyboard.add_abbreviation('@@', 'example@email.com'),
keyboard.add_abbreviation('HH', 'SecondAbbreviation'),
]</code>
